module NumberFormatting
  def format_k(input)
    input = input.to_s if input.respond_to?(:to_s)

    if input.size > 3
      input[0, input.size - 3] + 'k'
    else
      input
    end
  end

  def format_dot(input)
    input = input.to_s if input.respond_to?(:to_s)

    output = ''
    i = 0
    input_reverse = input.reverse

    for c in 0..input.length - 1
      output += input_reverse[c]
      i += 1
      if i % 3 == 0 && c != (input.length - 1)
        output += '.'
        i = 0
      end
    end

    output.reverse
  end
end

Liquid::Template.register_filter(NumberFormatting)
