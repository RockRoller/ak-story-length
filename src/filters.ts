export type filterSet = {
    itemList: itemData[]
    container: HTMLElement
    controls: HTMLElement
    filter: Function
}

export type itemData = {
    name: string
    count: {
        main: number
        event: number
        total: number
    }
    startingOrder: number
    type: "npc" | "op"
    element: HTMLElement
    show: boolean
}

export function initFilterEventListeners(filterSet: filterSet) {
    filterSet.controls.querySelectorAll("input").forEach((e) => {
        e.addEventListener("input", () => {
            filterSet.filter()
        })
    })
    filterSet.controls.querySelectorAll("select").forEach((e) => {
        e.addEventListener("input", () => {
            filterSet.filter()
        })
    })
}

export function setItemList(filterSet: filterSet) {
    const defaultAvaImg = 'url("/img/avatars/unknown.png")'
    filterSet.container.querySelectorAll<HTMLElement>(".comparison-item").forEach((e, i) => {
        const avaImg = e.querySelector<HTMLElement>(".comparison-item__avatar")!.style.backgroundImage
        const countNodes = e.querySelectorAll<HTMLElement>(".comparison-item__count--exact")
        const count = {
            main: parseInt(countNodes[0]!.innerText),
            event: parseInt(countNodes[1]?.innerText),
        }

        filterSet.itemList.push({
            name: e.querySelector<HTMLElement>(".comparison-item__name")!.innerText,
            count: {
                main: count.main,
                event: count.event,
                total: isNaN(count.event) ? count.main : count.main + count.event,
            },
            startingOrder: i,
            type: avaImg == defaultAvaImg ? "npc" : "op",
            element: e,
            show: true,
        })
    })
}

export function filter(filterSet: filterSet) {
    const showOpCheckboxState = filterSet.controls.querySelector<HTMLInputElement>(".checkbox--operator")?.checked
    const showOp = showOpCheckboxState == null ? true : showOpCheckboxState
    const showNPCCheckboxState = filterSet.controls.querySelector<HTMLInputElement>(".checkbox--npc")?.checked
    const showNPC = showNPCCheckboxState == null ? true : showNPCCheckboxState
    const order = filterSet.controls.querySelector<HTMLSelectElement>(".order")!.value
    const search = filterSet.controls.querySelector<HTMLInputElement>(".search--text")!.value
    const minTotal = parseInt(filterSet.controls.querySelector<HTMLInputElement>(".search--min-total")!.value)

    filterSet.itemList.forEach((e) => {
        let include: boolean = false

        if (e.type == "npc" && showNPC) {
            include = true
        } else if (e.type == "op" && showOp) {
            include = true
        }
        if (!e.name.toLowerCase().includes(search.toLowerCase())) {
            include = false
        }
        if (e.count.total < minTotal) {
            include = false
        }
        e.show = include
    })
    switch (order) {
        case "none":
            filterSet.itemList.sort((a: itemData, b: itemData) => {
                if (a.startingOrder < b.startingOrder) return -1
                if (a.startingOrder > b.startingOrder) return 1
                return 0
            })
            break
        case "a-z":
            filterSet.itemList.sort((a: itemData, b: itemData) => {
                if (a.name.toLowerCase() < b.name.toLowerCase()) return -1
                if (a.name.toLowerCase() > b.name.toLowerCase()) return 1
                return 0
            })
            break
        case "z-a":
            filterSet.itemList.sort((a: itemData, b: itemData) => {
                if (a.name.toLowerCase() > b.name.toLowerCase()) return -1
                if (a.name.toLowerCase() < b.name.toLowerCase()) return 1
                return 0
            })
            break
        case "count-total":
            filterSet.itemList.sort((a: itemData, b: itemData) => {
                if (a.count.total > b.count.total) return -1
                if (a.count.total < b.count.total) return 1
                return 0
            })
            break
        case "count-event":
            filterSet.itemList.sort((a: itemData, b: itemData) => {
                if (a.count.event > b.count.event) return -1
                if (a.count.event < b.count.event) return 1
                return 0
            })
            break
        case "count-main":
            filterSet.itemList.sort((a: itemData, b: itemData) => {
                if (a.count.main > b.count.main) return -1
                if (a.count.main < b.count.main) return 1
                return 0
            })
            break
    }

    let visibleNodesCount: number = 0
    filterSet.itemList.forEach((e, i) => {
        if (e.show) {
            e.element.classList.remove("hidden")
            e.element.classList.remove("odd")
            e.element.style.order = i.toString()
            visibleNodesCount++
            if (visibleNodesCount % 2 != 0) {
                e.element.classList.add("odd")
            }
        } else {
            e.element.classList.add("hidden")
        }
    })
}
