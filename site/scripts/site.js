'use strict';

/*! *****************************************************************************
Copyright (c) Microsoft Corporation.

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.
***************************************************************************** */

function __rest(s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
}

const arrow = `<svg viewBox="0 0 24 24" fill="none"><path d="M9 5L16 12L9 19" fill="#1c1719" stroke="#1c1719" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/></svg>`;
const at = `<svg class="w-6 h-6" fill="currentColor" viewBox="0 0 20 20"><path fill-rule="evenodd" d="M14.243 5.757a6 6 0 10-.986 9.284 1 1 0 111.087 1.678A8 8 0 1118 10a3 3 0 01-4.8 2.401A4 4 0 1114 10a1 1 0 102 0c0-1.537-.586-3.07-1.757-4.243zM12 10a2 2 0 10-4 0 2 2 0 004 0z" clip-rule="evenodd"></path></svg>`;
const avatar = `<svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5.121 17.804A13.937 13.937 0 0112 16c2.5 0 4.847.655 6.879 1.804M15 10a3 3 0 11-6 0 3 3 0 016 0zm6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg>`;
const bookmark = `<svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 5a2 2 0 012-2h10a2 2 0 012 2v16l-7-3.5L5 21V5z"></path></svg>`;
const bookmarkFilled = `<svg class="w-6 h-6" fill="currentColor" viewBox="0 0 20 20"><path d="M5 4a2 2 0 012-2h6a2 2 0 012 2v14l-5-2.5L5 18V4z"></path></svg>`;
const change = `<svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8 7h12m0 0l-4-4m4 4l-4 4m0 6H4m0 0l4 4m-4-4l4-4"></path></svg>`;
const chat = `<svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8 12h.01M12 12h.01M16 12h.01M21 12c0 4.418-4.03 8-9 8a9.863 9.863 0 01-4.255-.949L3 20l1.395-3.72C3.512 15.042 3 13.574 3 12c0-4.418 4.03-8 9-8s9 3.582 9 8z"></path></svg>`;
const close = `<svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12"></path></svg>`;
const collection = `<svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 11H5m14 0a2 2 0 012 2v6a2 2 0 01-2 2H5a2 2 0 01-2-2v-6a2 2 0 012-2m14 0V9a2 2 0 00-2-2M5 11V9a2 2 0 012-2m0 0V5a2 2 0 012-2h6a2 2 0 012 2v2M7 7h10"></path></svg>`;
const copy = `<svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8 16H6a2 2 0 01-2-2V6a2 2 0 012-2h8a2 2 0 012 2v2m-6 12h8a2 2 0 002-2v-8a2 2 0 00-2-2h-8a2 2 0 00-2 2v8a2 2 0 002 2z"></path></svg>`;
const documentAdd = `<svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 13h6m-3-3v6m5 5H7a2 2 0 01-2-2V5a2 2 0 012-2h5.586a1 1 0 01.707.293l5.414 5.414a1 1 0 01.293.707V19a2 2 0 01-2 2z"></path></svg>`;
const documentFilled = `<svg class="w-6 h-6" fill="currentColor" viewBox="0 0 20 20" ><path fill-rule="evenodd" d="M4 4a2 2 0 012-2h4.586A2 2 0 0112 2.586L15.414 6A2 2 0 0116 7.414V16a2 2 0 01-2 2H6a2 2 0 01-2-2V4z" clip-rule="evenodd"></path></svg>`;
const download = `<svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 16v1a3 3 0 003 3h10a3 3 0 003-3v-1m-4-4l-4 4m0 0l-4-4m4 4V4"></path></svg>`;
const externalLink = `<svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 6H6a2 2 0 00-2 2v10a2 2 0 002 2h10a2 2 0 002-2v-4M14 4h6m0 0v6m0-6L10 14"></path></svg>`;
const image = `<svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 16l4.586-4.586a2 2 0 012.828 0L16 16m-2-2l1.586-1.586a2 2 0 012.828 0L20 14m-6-6h.01M6 20h12a2 2 0 002-2V6a2 2 0 00-2-2H6a2 2 0 00-2 2v12a2 2 0 002 2z"></path></svg>`;
const message = `<svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8 12h.01M12 12h.01M16 12h.01M21 12c0 4.418-4.03 8-9 8a9.863 9.863 0 01-4.255-.949L3 20l1.395-3.72C3.512 15.042 3 13.574 3 12c0-4.418 4.03-8 9-8s9 3.582 9 8z"></path></svg>`;
const refresh = `<svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 4v5h.582m15.356 2A8.001 8.001 0 004.582 9m0 0H9m11 11v-5h-.581m0 0a8.003 8.003 0 01-15.357-2m15.357 2H15"></path></svg>`;
const search = `<svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"></path></svg>`;
const setting = `<svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10.325 4.317c.426-1.756 2.924-1.756 3.35 0a1.724 1.724 0 002.573 1.066c1.543-.94 3.31.826 2.37 2.37a1.724 1.724 0 001.065 2.572c1.756.426 1.756 2.924 0 3.35a1.724 1.724 0 00-1.066 2.573c.94 1.543-.826 3.31-2.37 2.37a1.724 1.724 0 00-2.572 1.065c-.426 1.756-2.924 1.756-3.35 0a1.724 1.724 0 00-2.573-1.066c-1.543.94-3.31-.826-2.37-2.37a1.724 1.724 0 00-1.065-2.572c-1.756-.426-1.756-2.924 0-3.35a1.724 1.724 0 001.066-2.573c-.94-1.543.826-3.31 2.37-2.37.996.608 2.296.07 2.572-1.065z"></path><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"></path></svg>`;
const trash = `<svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"></path></svg>`;
const warning = `<svg class="w-6 h-6" fill="currentColor" viewBox="0 0 20 20"><path fill-rule="evenodd" d="M8.257 3.099c.765-1.36 2.722-1.36 3.486 0l5.58 9.92c.75 1.334-.213 2.98-1.742 2.98H4.42c-1.53 0-2.493-1.646-1.743-2.98l5.58-9.92zM11 13a1 1 0 11-2 0 1 1 0 012 0zm-1-8a1 1 0 00-1 1v3a1 1 0 002 0V6a1 1 0 00-1-1z" clip-rule="evenodd"></path></svg>`;
const chevronDown = `<svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7"></path></svg>`;
const edit = `<svg class="w-6 h-6" fill="currentColor" viewBox="0 0 20 20" ><path d="M17.414 2.586a2 2 0 00-2.828 0L7 10.172V13h2.828l7.586-7.586a2 2 0 000-2.828z"></path><path fill-rule="evenodd" d="M2 6a2 2 0 012-2h4a1 1 0 010 2H4v10h10v-4a1 1 0 112 0v4a2 2 0 01-2 2H4a2 2 0 01-2-2V6z" clip-rule="evenodd"></path></svg>`;
const upload = `<svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 16v1a3 3 0 003 3h10a3 3 0 003-3v-1m-4-8l-4-4m0 0L8 8m4-4v12"></path></svg>`;
const pause = `<svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 9v6m4-6v6m7-3a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg>`;
const play = `<svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M14.752 11.168l-3.197-2.132A1 1 0 0010 9.87v4.263a1 1 0 001.555.832l3.197-2.132a1 1 0 000-1.664z"></path><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M21 12a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg>`;

var icons = /*#__PURE__*/Object.freeze({
    __proto__: null,
    arrow: arrow,
    at: at,
    avatar: avatar,
    bookmark: bookmark,
    bookmarkFilled: bookmarkFilled,
    change: change,
    chat: chat,
    close: close,
    collection: collection,
    copy: copy,
    documentAdd: documentAdd,
    documentFilled: documentFilled,
    download: download,
    externalLink: externalLink,
    image: image,
    message: message,
    refresh: refresh,
    search: search,
    setting: setting,
    trash: trash,
    warning: warning,
    chevronDown: chevronDown,
    edit: edit,
    upload: upload,
    pause: pause,
    play: play
});

class MissingElementException extends Error {
    constructor(message) {
        super(message);
        this.name = "MissingElementException";
    }
}

function ensuredSelector(query, parent = document) {
    const element = parent.querySelector(query);
    if (!element) {
        throw new MissingElementException(`No element found for query: "${query}"`);
    }
    return element;
}

function createElementFromHTML(html) {
    return createElement("div", {
        attributes: {
            innerHTML: html,
        },
    }).firstChild;
}
function createElement(tagName, options = {}) {
    const { attributes = {}, style = {}, className = "", children = [] } = options;
    const element = document.createElement(tagName);
    Object.assign(element, Object.assign(Object.assign({}, attributes), { className: `rr-lib ${className}` }));
    Object.assign(element.style, style);
    element.append(...children);
    return element;
}
function insertStyleTag(styleText) {
    ensuredSelector("head").append(createElement("style", {
        attributes: {
            innerHTML: styleText,
        },
    }));
}
function createIcon(name, options = {}) {
    const { size = 16 } = options, rest = __rest(options, ["size"]);
    const element = createElementFromHTML(icons[name]);
    Object.assign(element.style, Object.assign({ width: `${size}px`, height: `${size}px` }, rest));
    return element;
}

class ToggleInput extends HTMLDivElement {
    constructor(clickFunction) {
        super();
        this.classList.add("toggle");
        this.addEventListener("click", clickFunction);
        this.append(createElement("div", {
            className: "toggle__background",
        }));
        this.append(createElement("div", {
            className: "toggle__knob",
        }));
    }
}

class BaseModalButton extends HTMLButtonElement {
    constructor(label, classModifier, clickFunction) {
        super();
        this.classList.add("rr-modal__button", `rr-modal__button--${classModifier}`);
        this.innerText = label;
        this.addEventListener("click", clickFunction);
    }
}

class BaseModal extends HTMLDivElement {
    constructor(modalWrapper, modalHeader) {
        super();
        this.buttonContainer = createElement("div", {
            className: "rr-modal__buttons-container",
            style: {
                display: "flex",
                gap: "8px",
            },
        });
        this.contentBox = createElement("div", {
            style: {
                gap: "inherit",
                display: "flex",
                flexDirection: "column",
                margin: "0 8px",
                maxHeight: "60vh",
                overflowY: "auto",
            },
            className: "rr-modal__content-box",
        });
        modalWrapper.append(this);
        this.classList.add("rr-modal");
        this.addEventListener("click", (e) => {
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
        });
        this.header = modalHeader;
        this.closeButton = new BaseModalButton("close", "close", () => {
            document.body.classList.remove("freeze");
            modalWrapper.remove();
        });
        this.style.margin = "auto";
        this.buttonContainer.append(this.closeButton);
        this.append(this.header);
        this.append(this.contentBox);
        this.append(this.buttonContainer);
    }
    addContent(content) {
        this.contentBox.append(content);
        content.scrollIntoView();
    }
    addModalButton(button) {
        this.buttonContainer.prepend(button);
    }
    addIconButton(iconName, tooltip, eventFunction) {
        this.header.addIconButton(iconName, tooltip, eventFunction);
    }
    setHeaderTitle(headerTitle) {
        this.header.setHeaderTitle(headerTitle);
    }
}

class BaseModalHeader extends HTMLDivElement {
    constructor(headerTitle) {
        super();
        this.iconContainer = createElement("div", {
            className: "rr-modal__header-buttons",
            style: {
                display: "flex",
                alignItems: "center",
                gap: "8px",
            },
        });
        this.classList.add("rr-modal__header");
        this.headerLabel = createElement("span", {
            className: "rr-modal__header-label",
            attributes: {
                innerText: headerTitle,
            },
        });
        this.style.display = "flex";
        this.style.justifyContent = "space-between";
        this.style.alignItems = "center";
        this.append(this.headerLabel);
        this.append(this.iconContainer);
    }
    addIconButton(iconName, tooltip, clickFunction) {
        this.iconContainer.append(createElement("div", {
            style: {
                display: "flex",
                cursor: "pointer",
            },
            attributes: {
                title: tooltip,
                onclick: clickFunction,
            },
            children: [createIcon(iconName)],
        }));
    }
    setHeaderTitle(headerTitle) {
        this.headerLabel.innerText = headerTitle;
    }
}

class ModalWrapper extends HTMLDivElement {
    constructor() {
        super();
        insertStyleTag(`.freeze { overflow-y: hidden }`);
        this.classList.add("modal-wrapper");
        this.addEventListener("click", () => {
            document.body.classList.remove("freeze");
            this.remove();
        });
        window.addEventListener("keydown", (e) => {
            if (e.key === "Escape") {
                document.body.classList.remove("freeze");
                this.remove();
            }
        });
        document.body.classList.add("freeze");
        this.style.display = "flex";
        this.style.position = "fixed";
        this.style.top = "0";
        this.style.left = "0";
        this.style.width = "100%";
        this.style.height = "100%";
        this.style.zIndex = "1000";
        this.style.backgroundColor = "rgba(0, 0, 0, 0.5)";
    }
}

const osuTheme = {
    successColor: "#b3d944",
    failureColor: "#d94444",
    enabledColor: "#b3d944",
    disabledColor: "#ed1221",
    normalTextColor: "rgba(255, 255, 255, 0.85)",
    notificationBackground: "#1c1719",
    secondaryBackground: "#382E32",
    modalBackground: "#54454C",
    inputMinWidth: "250px",
    osuPink: "#ff66ab",
};

class OsuModal extends BaseModal {
    constructor(modalWrapper, modalHeader) {
        super(modalWrapper, modalHeader);
        this.closeButton.classList.add("btn-osu-big", `btn-osu-big--forum-secondary`);
        this.closeButton.innerText = "Close";
        this.style.padding = "16px";
        this.style.minWidth = "400px";
        this.style.display = "flex";
        this.style.flexDirection = "column";
        this.style.gap = "8px";
        this.style.borderRadius = "4px";
        this.style.backgroundColor = osuTheme.modalBackground;
    }
}

class OsuModalButton extends BaseModalButton {
    constructor(label, classModifier, clickFunction, buttonType = "primary") {
        super(label, classModifier, clickFunction);
        this.classList.add("btn-osu-big", `btn-osu-big--forum-${buttonType}`);
    }
}

class OsuModalHeader extends BaseModalHeader {
    constructor(headerTitle) {
        super(headerTitle);
        this.style.fontWeight = "700";
        this.style.fontSize = "1.25em";
    }
}

function initComponents() {
    customElements.define("custom-input-toggle", ToggleInput, { extends: "div" });
    customElements.define("modal-wrapper", ModalWrapper, { extends: "div" });
    customElements.define("base-modal", BaseModal, { extends: "div" });
    customElements.define("base-modal-button", BaseModalButton, { extends: "button" });
    customElements.define("base-modal-header", BaseModalHeader, { extends: "div" });
    customElements.define("osu-modal", OsuModal, { extends: "div" });
    customElements.define("osu-modal-button", OsuModalButton, { extends: "button" });
    customElements.define("osu-modal-header", OsuModalHeader, { extends: "div" });
}

function initFilterEventListeners(filterSet) {
    filterSet.controls.querySelectorAll("input").forEach((e) => {
        e.addEventListener("input", () => {
            filterSet.filter();
        });
    });
    filterSet.controls.querySelectorAll("select").forEach((e) => {
        e.addEventListener("input", () => {
            filterSet.filter();
        });
    });
}
function setItemList(filterSet) {
    const defaultAvaImg = 'url("/img/avatars/unknown.png")';
    filterSet.container.querySelectorAll(".comparison-item").forEach((e, i) => {
        var _a;
        const avaImg = e.querySelector(".comparison-item__avatar").style.backgroundImage;
        const countNodes = e.querySelectorAll(".comparison-item__count--exact");
        const count = {
            main: parseInt(countNodes[0].innerText),
            event: parseInt((_a = countNodes[1]) === null || _a === void 0 ? void 0 : _a.innerText),
        };
        filterSet.itemList.push({
            name: e.querySelector(".comparison-item__name").innerText,
            count: {
                main: count.main,
                event: count.event,
                total: isNaN(count.event) ? count.main : count.main + count.event,
            },
            startingOrder: i,
            type: avaImg == defaultAvaImg ? "npc" : "op",
            element: e,
            show: true,
        });
    });
}
function filter(filterSet) {
    var _a, _b;
    const showOpCheckboxState = (_a = filterSet.controls.querySelector(".checkbox--operator")) === null || _a === void 0 ? void 0 : _a.checked;
    const showOp = showOpCheckboxState == null ? true : showOpCheckboxState;
    const showNPCCheckboxState = (_b = filterSet.controls.querySelector(".checkbox--npc")) === null || _b === void 0 ? void 0 : _b.checked;
    const showNPC = showNPCCheckboxState == null ? true : showNPCCheckboxState;
    const order = filterSet.controls.querySelector(".order").value;
    const search = filterSet.controls.querySelector(".search--text").value;
    const minTotal = parseInt(filterSet.controls.querySelector(".search--min-total").value);
    filterSet.itemList.forEach((e) => {
        let include = false;
        if (e.type == "npc" && showNPC) {
            include = true;
        }
        else if (e.type == "op" && showOp) {
            include = true;
        }
        if (!e.name.toLowerCase().includes(search.toLowerCase())) {
            include = false;
        }
        if (e.count.total < minTotal) {
            include = false;
        }
        e.show = include;
    });
    switch (order) {
        case "none":
            filterSet.itemList.sort((a, b) => {
                if (a.startingOrder < b.startingOrder)
                    return -1;
                if (a.startingOrder > b.startingOrder)
                    return 1;
                return 0;
            });
            break;
        case "a-z":
            filterSet.itemList.sort((a, b) => {
                if (a.name.toLowerCase() < b.name.toLowerCase())
                    return -1;
                if (a.name.toLowerCase() > b.name.toLowerCase())
                    return 1;
                return 0;
            });
            break;
        case "z-a":
            filterSet.itemList.sort((a, b) => {
                if (a.name.toLowerCase() > b.name.toLowerCase())
                    return -1;
                if (a.name.toLowerCase() < b.name.toLowerCase())
                    return 1;
                return 0;
            });
            break;
        case "count-total":
            filterSet.itemList.sort((a, b) => {
                if (a.count.total > b.count.total)
                    return -1;
                if (a.count.total < b.count.total)
                    return 1;
                return 0;
            });
            break;
        case "count-event":
            filterSet.itemList.sort((a, b) => {
                if (a.count.event > b.count.event)
                    return -1;
                if (a.count.event < b.count.event)
                    return 1;
                return 0;
            });
            break;
        case "count-main":
            filterSet.itemList.sort((a, b) => {
                if (a.count.main > b.count.main)
                    return -1;
                if (a.count.main < b.count.main)
                    return 1;
                return 0;
            });
            break;
    }
    let visibleNodesCount = 0;
    filterSet.itemList.forEach((e, i) => {
        if (e.show) {
            e.element.classList.remove("hidden");
            e.element.classList.remove("odd");
            e.element.style.order = i.toString();
            visibleNodesCount++;
            if (visibleNodesCount % 2 != 0) {
                e.element.classList.add("odd");
            }
        }
        else {
            e.element.classList.add("hidden");
        }
    });
}

if (document.readyState === "loading") {
    document.addEventListener("DOMContentLoaded", main);
}
else {
    main();
}
function main() {
    initDetailsEventListeners();
    calcWPM();
    const filterSetOperatorWords = {
        itemList: [],
        container: ensuredSelector(".comparison--operator.comparison--words"),
        controls: ensuredSelector(".comparison-header__controls.words"),
        filter: () => filter(filterSetOperatorWords),
    };
    setItemList(filterSetOperatorWords);
    initFilterEventListeners(filterSetOperatorWords);
    filterSetOperatorWords.filter();
    const filterSetOperatorCharacters = {
        itemList: [],
        container: ensuredSelector(".comparison--operator.comparison--characters"),
        controls: ensuredSelector(".comparison-header__controls.characters"),
        filter: () => filter(filterSetOperatorCharacters),
    };
    setItemList(filterSetOperatorCharacters);
    initFilterEventListeners(filterSetOperatorCharacters);
    filterSetOperatorCharacters.filter();
}
function initDetailsEventListeners() {
    initComponents();
    document.querySelectorAll(".act").forEach((e) => {
        e.querySelector(".act__details-button").addEventListener("click", () => {
            const data = JSON.parse(e.dataset.stages);
            const name = e.querySelector(".act__title").innerText;
            const modalWrapper = new ModalWrapper();
            const modal = new BaseModal(modalWrapper, new BaseModalHeader(name));
            data.forEach((stage) => {
                const wordNode = createElement("span", {
                    className: "stage-list-item__length stage-list-item__length--words",
                    attributes: { innerText: stage.count.words.toString() },
                });
                wordNode.dataset.words = stage.count.words.toString();
                const charNode = createElement("span", {
                    className: "stage-list-item__length stage-list-item__length--chars",
                    attributes: { innerText: stage.count.chars.toString() },
                });
                charNode.dataset.characters = stage.count.chars.toString();
                modal.addContent(createElement("div", {
                    className: "stage-list-item",
                    children: [
                        createElement("div", {
                            className: "stage-list-item__title",
                            children: [
                                createElement("span", { className: "stage-list-item__title-code", attributes: { innerText: `${stage.storyCode} ${stage.avgTag.replace("Operation", "")}` } }),
                                createElement("span", { className: "stage-list-item__title-name", attributes: { innerText: stage.storyName } }),
                            ],
                        }),
                        charNode,
                        wordNode,
                    ],
                }));
            });
            document.body.append(modalWrapper);
            calcWPM();
        });
    });
}
function calcWPM() {
    document.querySelectorAll("*[data-words]").forEach((e) => {
        if (e.dataset.readTime || e.dataset.words == "") {
            return;
        }
        let minutes = Math.round(parseInt(e.dataset.words) / 200);
        let hours = 0;
        while (minutes > 60) {
            minutes -= 60;
            hours++;
        }
        e.dataset.readTime = hours == 0 ? `${minutes} minutes` : `${hours} hours ${minutes} minutes`;
    });
}
