# Ak Story Length

Just a small page I wrote for fun, after seeing [this post on reddit](https://www.reddit.com/r/arknights/comments/naqi36/word_count_of_all_event_and_main_story_so_far/) and wanting to know the english stats, as well as having an updated version. If you find any issues or have ideas for new features, please open an [issue](https://gitlab.com/RockRoller/ak-story-length/-/issues) or contact me via Discord (RockRoller#1909).

# Updating Game Data

|            directory            |                                     usage                                     |
| :-----------------------------: | :---------------------------------------------------------------------------: |
|        /story/obt/memory        |                               character stories                               |
|         /story/obt/main         |                                  main story                                   |
|      /story/obt/guide/beg       | guide files for game start, only guides that are considered part of the story |
|        /story/activities        |                    event stories, each folder is one event                    |
|   /story/character_table.json   |                                character info                                 |
|   /story/charword_table.json    |                             character voice lines                             |
| /story/handbook_info_table.json |                             character trust notes                             |
| /story/story_review_table.json  |                                  story info                                   |

Data from: https://github.com/Kengxxiao/ArknightsGameData_YoStar

1. replace table files with most recent ones
2. add missing stories (simply copy paste with 'skip existing files')
   Currently excluding:
   - act1lock, as its related to "Interlocking Competition" which doesn't appear anywhere.. (ignored via gitignore)
   - act17d7, as its related to Emperors april fools event which doesn't appear anywhere.. (ignored via gitignore)
3. `bundle exec jekyll serve` to start local site serve
4. go to /json (http://127.0.0.1:4000/json)
5. use "Get JSON" button
6. paste files generated into site/\_data
7. add cover image for new events (event_id.png) as well as new operator images (strip: " ", ".", "'")
