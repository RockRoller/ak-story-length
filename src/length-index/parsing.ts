import { downloadUrl } from "../../typescript-lib/src/shared/common"
import { characterTable, characterWordTable, count, handbookInfoTable, infoUnlockDatas, remappedCharacterStoriesEntry, storyReviewTable, storyReviewTableEntry } from "../types"

/**
 * main function of the index building.
 * this function queries over all data and downloads the needed .json files in the end
 */
export async function downloadChapterData() {
    const storyReviewTable: storyReviewTable = await parseTable<storyReviewTable>("/story/story_review_table.json")
    const handbookInfoTable: handbookInfoTable = await parseTable<handbookInfoTable>("/story/handbook_info_table.json")
    const characterTable: characterTable = await parseTable<characterTable>("/story/character_table.json")
    const characterWordTable = (await parseTable<characterWordTable>("/story/charword_table.json")).charWords
    const characters = new Map<string, { main: count; event: count }>()
    const mainStory: storyReviewTableEntry[] = []
    const eventStory: storyReviewTableEntry[] = []
    const characterStory: storyReviewTableEntry[] = []
    const trustNoteData: { characterName: string; count: count }[] = []
    const voiceLineData = new Map<string, { characterName: string; count: count }>()
    const remappedCharacterStories = new Map<string, remappedCharacterStoriesEntry>()
    // all story links
    const linkMap = new Map<string, string>() // key | link

    // get all story files
    document.querySelectorAll<HTMLAnchorElement>(".list__item").forEach((e) => {
        linkMap.set(e.innerText.replace(".txt", ""), e.href)
    })
    // iterate over all stories and get their data
    for (const [storyReviewTableEntryKey, storyReviewTableEntry] of Object.entries(storyReviewTable)) {
        // one chapter = one event | one main story chapter | one character story
        const chapter: storyReviewTableEntry = {
            id: storyReviewTableEntry.id,
            name: storyReviewTableEntry.name,
            entryType: storyReviewTableEntry.entryType,
            infoUnlockDatas: [],
        }
        // get basic chapter metadata and assing to correct list
        // main story
        if (chapter.entryType == "MAINLINE") {
            mainStory.push(chapter)
            // event
        } else if (chapter.entryType == "ACTIVITY" || chapter.entryType == "MINI_ACTIVITY") {
            eventStory.push(chapter)
            // anything else *should* be character stories
        } else {
            characterStory.push(chapter)
            let operatorID = ""
            // get operator and sortID of character story
            loop1: for (const [, entry] of Object.entries(handbookInfoTable.handbookDict)) {
                for (const storyEntry of entry.handbookAvgList) {
                    if (storyEntry.storySetId == chapter.id) {
                        operatorID = entry.charID
                        chapter.sortId = storyEntry.sortId
                        break loop1
                    }
                }
            }
            chapter.operatorName = getCharacterNameByID(operatorID)
        }

        // get data for each stage of chapter
        for (const infoUnlockDatas of storyReviewTableEntry.infoUnlockDatas) {
            const story: infoUnlockDatas = {
                storyCode: infoUnlockDatas.storyCode,
                storyName: infoUnlockDatas.storyName,
                storyTxt: infoUnlockDatas.storyTxt,
                avgTag: infoUnlockDatas.avgTag,
                count: {
                    words: 0,
                    chars: 0,
                },
            }
            chapter.infoUnlockDatas.push(story)
            const link = linkMap.get(story.storyTxt)
            if (!link) {
                console.log(`Missing file for: ${story.storyTxt}`)
                continue
            }

            const text = await (await fetch(link)).text()

            const regex = /\[name="(.+)"\]\s*(.*)/gm
            let m // [full match, name, content]

            while ((m = regex.exec(text)) !== null) {
                // This is necessary to avoid infinite loops with zero-width matches
                if (m.index === regex.lastIndex) {
                    regex.lastIndex++
                }
                const character = m[1]
                const content = m[2]
                // add to character map
                if (!characters.has(character)) {
                    characters.set(character, { main: { words: 0, chars: 0 }, event: { words: 0, chars: 0 } })
                }
                const current = characters.get(character)!
                if (chapter.entryType == "MAINLINE") {
                    characters.set(character, { main: { words: current.main.words + content.split(" ").length, chars: current.main.chars + content.length }, event: current.event })
                } else if (chapter.entryType == "ACTIVITY" || chapter.entryType == "MINI_ACTIVITY") {
                    characters.set(character, { main: current.main, event: { words: current.event.words + content.split(" ").length, chars: current.event.chars + content.length } })
                }
                // add to chapters count
                story.count.chars += content.length
                story.count.words += content.split(" ").length
            }
        }
    }
    // iterate over all trust notes and get their data
    for (const [, entry] of Object.entries(handbookInfoTable.handbookDict)) {
        const charName = getCharacterNameByID(entry.charID)
        // 5 npcs are listed in this file since they appear on the diagram, these will not come up with a name and will get skipped trough this
        if (charName == "") {
            continue
        }
        const count: count = { words: 0, chars: 0 }
        entry.storyTextAudio.forEach((e) => {
            e.stories.forEach((story) => {
                count.words += story.storyText.split(" ").length
                count.chars += story.storyText.length
            })
        })
        trustNoteData.push({
            characterName: charName,
            count: count,
        })
    }
    // iterate over all voice lines and get their data
    for (const [, entry] of Object.entries(characterWordTable)) {
        const charName = getCharacterNameByID(entry.charId)
        const count: count = {
            words: entry.voiceText.split(" ").length,
            chars: entry.voiceText.length,
        }

        // add do map if missing
        if (!voiceLineData.has(charName)) {
            voiceLineData.set(charName, {
                characterName: charName,
                count: {
                    words: 0,
                    chars: 0,
                },
            })
        }
        const currentCount = voiceLineData.get(charName)!.count

        voiceLineData.set(charName, {
            characterName: charName,
            count: {
                words: count.words + currentCount.words,
                chars: count.chars + currentCount.chars,
            },
        })
    }

    // remap character story data for easier usage in jekyll
    for (const entry of characterStory) {
        if (!remappedCharacterStories.has(entry.operatorName!)) {
            remappedCharacterStories.set(entry.operatorName!, {
                characterName: entry.operatorName!,
                stories: [],
            })
        }
        let wordCount = 0
        let charCount = 0
        entry.infoUnlockDatas.forEach((e) => {
            wordCount += e.count.words
            charCount += e.count.chars
        })
        const storyEntry = {
            id: entry.sortId!,
            name: entry.name,
            count: { words: wordCount, chars: charCount },
        }
        const currentState = remappedCharacterStories.get(entry.operatorName!)!
        currentState.stories.push(storyEntry)
        remappedCharacterStories.set(entry.operatorName!, currentState)
    }
    downloadUrl(`data:text/plain;charset=utf-8, ${encodeURIComponent(JSON.stringify(Array.from(characters.entries())))}`, "lengths_per_operator.json")
    downloadUrl(`data:text/plain;charset=utf-8, ${encodeURIComponent(JSON.stringify(mainStory))}`, "lengths_main_story.json")
    downloadUrl(`data:text/plain;charset=utf-8, ${encodeURIComponent(JSON.stringify(eventStory))}`, "lengths_events.json")
    downloadUrl(`data:text/plain;charset=utf-8, ${encodeURIComponent(JSON.stringify(Array.from(remappedCharacterStories.values())))}`, "lengths_operator_stories.json")
    downloadUrl(`data:text/plain;charset=utf-8, ${encodeURIComponent(JSON.stringify(trustNoteData))}`, "lengths_operator_trust_notes.json")
    downloadUrl(`data:text/plain;charset=utf-8, ${encodeURIComponent(JSON.stringify(Array.from(voiceLineData.values())))}`, "lengths_operator_voice_lines.json")

    /**
     * gets the character name of an operator based on their ID from the character_table
     * @param id internal game id of the character
     * @returns character name
     */
    function getCharacterNameByID(id: string) {
        let operatorName = ""

        loop: for (const [characterId, characterEntry] of Object.entries(characterTable)) {
            if (characterId == id) {
                operatorName = characterEntry.name
                break loop
            }
        }

        return operatorName
    }
}

/**
 * parse arknights table
 * @param url link to table json file
 * @returns table as js object
 */
async function parseTable<T>(url: string) {
    return JSON.parse(await (await fetch(url)).text()) as T
}
