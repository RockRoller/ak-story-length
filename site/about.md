---
layout: master
highlight: /about
title: About
class: about
---

{% include components/heading.html name="About" %}

Just a small page I wrote for fun, after seeing [this post on reddit](https://www.reddit.com/r/arknights/comments/naqi36/word_count_of_all_event_and_main_story_so_far/) and wanting to know the english stats, as well as having an updated version. If you find any issues or have ideas for new features, please open an [issue](https://gitlab.com/RockRoller/ak-story-length/-/issues) or contact me via Discord (RockRoller#1909).

All reading time estimates on the website use 200 words per minute for their calculation, your own speed may vary.

{% include components/heading.html name="Privacy and Data Collection" %}

This website is made with [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/). I do not track your visit or activity on this website, and do not make use of any cookies. Third-party services - i.e. GitLab and Cloudflare - may be tracking some statistics about your visit of the website.

{% include components/heading.html name="Credits" %}

|                 |                                                                 |
| :-------------: | :-------------------------------------------------------------: |
|      Font       |        [Inter](https://fonts.google.com/specimen/Inter)         |
|   GitLab Icon   |                  [GitLab](https://gitlab.com)                   |
|  Privacy Icon   |               [Heroicons](https://heroicons.dev/)               |
| Operator Images | Creators of the game; [HyperGryph](https://www.hypergryph.com/) |
|  Anything else  |           [RockRoller](https://gitlab.com/RockRoller)           |
