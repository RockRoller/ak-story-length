'use strict';

/*! *****************************************************************************
Copyright (c) Microsoft Corporation.

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.
***************************************************************************** */

function __awaiter(thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
}

function createElement(tagName, options = {}) {
    const { attributes = {}, style = {}, className = "", children = [] } = options;
    const element = document.createElement(tagName);
    Object.assign(element, Object.assign(Object.assign({}, attributes), { className: `rr-lib ${className}` }));
    Object.assign(element.style, style);
    element.append(...children);
    return element;
}

function downloadUrl(url, filename = Date.now().toString()) {
    return __awaiter(this, void 0, void 0, function* () {
        const blobUrl = URL.createObjectURL(yield (yield fetch(url)).blob());
        const anchor = createElement("a", {
            attributes: {
                href: blobUrl,
                download: filename,
            },
        });
        document.body.append(anchor);
        anchor.click();
        anchor.remove();
        URL.revokeObjectURL(blobUrl);
    });
}

function downloadChapterData() {
    return __awaiter(this, void 0, void 0, function* () {
        const storyReviewTable = yield parseTable("/story/story_review_table.json");
        const handbookInfoTable = yield parseTable("/story/handbook_info_table.json");
        const characterTable = yield parseTable("/story/character_table.json");
        const characterWordTable = (yield parseTable("/story/charword_table.json")).charWords;
        const characters = new Map();
        const mainStory = [];
        const eventStory = [];
        const characterStory = [];
        const trustNoteData = [];
        const voiceLineData = new Map();
        const remappedCharacterStories = new Map();
        const linkMap = new Map();
        document.querySelectorAll(".list__item").forEach((e) => {
            linkMap.set(e.innerText.replace(".txt", ""), e.href);
        });
        for (const [storyReviewTableEntryKey, storyReviewTableEntry] of Object.entries(storyReviewTable)) {
            const chapter = {
                id: storyReviewTableEntry.id,
                name: storyReviewTableEntry.name,
                entryType: storyReviewTableEntry.entryType,
                infoUnlockDatas: [],
            };
            if (chapter.entryType == "MAINLINE") {
                mainStory.push(chapter);
            }
            else if (chapter.entryType == "ACTIVITY" || chapter.entryType == "MINI_ACTIVITY") {
                eventStory.push(chapter);
            }
            else {
                characterStory.push(chapter);
                let operatorID = "";
                loop1: for (const [, entry] of Object.entries(handbookInfoTable.handbookDict)) {
                    for (const storyEntry of entry.handbookAvgList) {
                        if (storyEntry.storySetId == chapter.id) {
                            operatorID = entry.charID;
                            chapter.sortId = storyEntry.sortId;
                            break loop1;
                        }
                    }
                }
                chapter.operatorName = getCharacterNameByID(operatorID);
            }
            for (const infoUnlockDatas of storyReviewTableEntry.infoUnlockDatas) {
                const story = {
                    storyCode: infoUnlockDatas.storyCode,
                    storyName: infoUnlockDatas.storyName,
                    storyTxt: infoUnlockDatas.storyTxt,
                    avgTag: infoUnlockDatas.avgTag,
                    count: {
                        words: 0,
                        chars: 0,
                    },
                };
                chapter.infoUnlockDatas.push(story);
                const link = linkMap.get(story.storyTxt);
                if (!link) {
                    console.log(`Missing file for: ${story.storyTxt}`);
                    continue;
                }
                const text = yield (yield fetch(link)).text();
                const regex = /\[name="(.+)"\]\s*(.*)/gm;
                let m;
                while ((m = regex.exec(text)) !== null) {
                    if (m.index === regex.lastIndex) {
                        regex.lastIndex++;
                    }
                    const character = m[1];
                    const content = m[2];
                    if (!characters.has(character)) {
                        characters.set(character, { main: { words: 0, chars: 0 }, event: { words: 0, chars: 0 } });
                    }
                    const current = characters.get(character);
                    if (chapter.entryType == "MAINLINE") {
                        characters.set(character, { main: { words: current.main.words + content.split(" ").length, chars: current.main.chars + content.length }, event: current.event });
                    }
                    else if (chapter.entryType == "ACTIVITY" || chapter.entryType == "MINI_ACTIVITY") {
                        characters.set(character, { main: current.main, event: { words: current.event.words + content.split(" ").length, chars: current.event.chars + content.length } });
                    }
                    story.count.chars += content.length;
                    story.count.words += content.split(" ").length;
                }
            }
        }
        for (const [, entry] of Object.entries(handbookInfoTable.handbookDict)) {
            const charName = getCharacterNameByID(entry.charID);
            if (charName == "") {
                continue;
            }
            const count = { words: 0, chars: 0 };
            entry.storyTextAudio.forEach((e) => {
                e.stories.forEach((story) => {
                    count.words += story.storyText.split(" ").length;
                    count.chars += story.storyText.length;
                });
            });
            trustNoteData.push({
                characterName: charName,
                count: count,
            });
        }
        for (const [, entry] of Object.entries(characterWordTable)) {
            const charName = getCharacterNameByID(entry.charId);
            const count = {
                words: entry.voiceText.split(" ").length,
                chars: entry.voiceText.length,
            };
            if (!voiceLineData.has(charName)) {
                voiceLineData.set(charName, {
                    characterName: charName,
                    count: {
                        words: 0,
                        chars: 0,
                    },
                });
            }
            const currentCount = voiceLineData.get(charName).count;
            voiceLineData.set(charName, {
                characterName: charName,
                count: {
                    words: count.words + currentCount.words,
                    chars: count.chars + currentCount.chars,
                },
            });
        }
        for (const entry of characterStory) {
            if (!remappedCharacterStories.has(entry.operatorName)) {
                remappedCharacterStories.set(entry.operatorName, {
                    characterName: entry.operatorName,
                    stories: [],
                });
            }
            let wordCount = 0;
            let charCount = 0;
            entry.infoUnlockDatas.forEach((e) => {
                wordCount += e.count.words;
                charCount += e.count.chars;
            });
            const storyEntry = {
                id: entry.sortId,
                name: entry.name,
                count: { words: wordCount, chars: charCount },
            };
            const currentState = remappedCharacterStories.get(entry.operatorName);
            currentState.stories.push(storyEntry);
            remappedCharacterStories.set(entry.operatorName, currentState);
        }
        downloadUrl(`data:text/plain;charset=utf-8, ${encodeURIComponent(JSON.stringify(Array.from(characters.entries())))}`, "lengths_per_operator.json");
        downloadUrl(`data:text/plain;charset=utf-8, ${encodeURIComponent(JSON.stringify(mainStory))}`, "lengths_main_story.json");
        downloadUrl(`data:text/plain;charset=utf-8, ${encodeURIComponent(JSON.stringify(eventStory))}`, "lengths_events.json");
        downloadUrl(`data:text/plain;charset=utf-8, ${encodeURIComponent(JSON.stringify(Array.from(remappedCharacterStories.values())))}`, "lengths_operator_stories.json");
        downloadUrl(`data:text/plain;charset=utf-8, ${encodeURIComponent(JSON.stringify(trustNoteData))}`, "lengths_operator_trust_notes.json");
        downloadUrl(`data:text/plain;charset=utf-8, ${encodeURIComponent(JSON.stringify(Array.from(voiceLineData.values())))}`, "lengths_operator_voice_lines.json");
        function getCharacterNameByID(id) {
            let operatorName = "";
            loop: for (const [characterId, characterEntry] of Object.entries(characterTable)) {
                if (characterId == id) {
                    operatorName = characterEntry.name;
                    break loop;
                }
            }
            return operatorName;
        }
    });
}
function parseTable(url) {
    return __awaiter(this, void 0, void 0, function* () {
        return JSON.parse(yield (yield fetch(url)).text());
    });
}

if (document.readyState === "loading") {
    document.addEventListener("DOMContentLoaded", main);
}
else {
    main();
}
function main() {
    initEventListeners();
}
function initEventListeners() {
    const button = document.querySelector(".button.json");
    const startingText = button.innerText;
    button.addEventListener("click", () => __awaiter(this, void 0, void 0, function* () {
        button.innerText = "Loading...";
        yield downloadChapterData();
        button.innerText = startingText;
    }));
}
