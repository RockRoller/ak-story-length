import { downloadChapterData } from "./parsing"

if (document.readyState === "loading") {
    // Loading hasn't finished yet
    document.addEventListener("DOMContentLoaded", main)
} else {
    // `DOMContentLoaded` has already fired
    main()
}

function main() {
    initEventListeners()
}

function initEventListeners() {
    const button = document.querySelector<HTMLElement>(".button.json")!
    const startingText = button.innerText
    button.addEventListener("click", async () => {
        button.innerText = "Loading..."
        await downloadChapterData()
        button.innerText = startingText
    })
}
