---
layout: master
highlight: /trivia
title: Trivia
class: trivia
---

<!-- prettier-ignore-start -->
{% assign count_total_chars = 0 %}
{% assign count_total_words = 0 %}
{% assign count_voice_chars = 0 %}
{% assign count_voice_words = 0 %}

{% for chapter in site.data.lengths_main_story %}
    {% for stage in chapter.infoUnlockDatas %}
        {% assign count_total_chars = count_total_chars | plus: stage.count.chars %}
        {% assign count_total_words = count_total_words | plus: stage.count.words %}
    {% endfor %}
{% endfor %}
{% for chapter in site.data.lengths_events %}
    {% for stage in chapter.infoUnlockDatas %}
        {% assign count_total_chars = count_total_chars | plus: stage.count.chars %}
        {% assign count_total_words = count_total_words | plus: stage.count.words %}
    {% endfor %}
{% endfor %}
{% for story in site.data.lengths_operator_voice_lines %}
    {% assign count_total_chars = count_total_chars | plus: story.count.chars %}
    {% assign count_voice_chars = count_voice_chars | plus: story.count.chars %}
    {% assign count_total_words = count_total_words | plus: story.count.words %}
    {% assign count_voice_words = count_voice_words | plus: story.count.words %}
{% endfor %}
{% for story in site.data.lengths_operator_trust_notes %}
    {% assign count_total_chars = count_total_chars | plus: story.count.chars %}
    {% assign count_total_words = count_total_words | plus: story.count.words %}
{% endfor %}
{% for character in site.data.lengths_operator_stories %}
    {% for story in character.stories %}
        {% assign count_total_chars = count_total_chars | plus: story.count.chars %}
        {% assign count_total_words = count_total_words | plus: story.count.words %}
    {% endfor %}
{% endfor %}

<!-- prettier-ignore-end -->

{% include components/heading.html name="Trivia" %}

Arknights has a total of {{ count_total_chars | format_dot }} characters and {{ count_total_words | format_dot }} words, putting it ahead of many well known books and book series:

|           Name           |                                            Included                                             |      Characters      |          Words           |
| :----------------------: | :---------------------------------------------------------------------------------------------: | :------------------: | :----------------------: | -------------------- | -------------------- | ------------------------ | ------------- |
|        Arknights         |       main story, event stories, character voice lines, operator stories and trust notes        | {{ count_total_chars |      format_dot }}       | {{ count_total_words | format_dot }}        |
|        Arknights         |                            Same as above, but excluding voice lines                             | {{ count_total_chars | minus: count_voice_chars | format_dot }}        | {{ count_total_words | minus: count_voice_words | format_dot }} |
|  A Song of Ice and Fire  | A Game of Thrones, A Clash of Kings, A Storm of Swords, A Feast for Crows, A Dance with Dragons |          -           |        1.770.000         |
|       Harry Potter       |                                          All 7 Volumes                                          |          -           |        1.084.625         |
|         Twilight         |                           Twilight, New Moon, Eclipse, Breaking Dawn                            |          -           |         587.000          |
|    Lord of the Rings     |         The Hobbit, The Fellowships of the Ring, The Two Towers, The Return of the King         |          -           |         576.500          |
| The Chronicles of Narnia |                                          All 7 Volumes                                          |          -           |         345.500          |
