import { initComponents } from "../typescript-lib/src/components/CustomElementDefinitions"
import { BaseModal } from "../typescript-lib/src/components/modal/base/BaseModal"
import { BaseModalHeader } from "../typescript-lib/src/components/modal/base/BaseModalHeader"
import { ModalWrapper } from "../typescript-lib/src/components/modal/ModalWrapper"
import { ensuredSelector } from "../typescript-lib/src/shared/common"
import { createElement } from "../typescript-lib/src/shared/dom"
import { filter, filterSet, initFilterEventListeners, setItemList } from "./filters"
import { infoUnlockDatas } from "./types"

if (document.readyState === "loading") {
    // Loading hasn't finished yet
    document.addEventListener("DOMContentLoaded", main)
} else {
    // `DOMContentLoaded` has already fired
    main()
}

function main() {
    // details buttons
    initDetailsEventListeners()
    calcWPM()

    // filters
    // story ops words
    const filterSetOperatorWords: filterSet = {
        itemList: [],
        container: ensuredSelector<HTMLElement>(".comparison--operator.comparison--words"),
        controls: ensuredSelector<HTMLElement>(".comparison-header__controls.words"),
        filter: () => filter(filterSetOperatorWords),
    }

    setItemList(filterSetOperatorWords)
    initFilterEventListeners(filterSetOperatorWords)
    filterSetOperatorWords.filter()

    // story ops chars
    const filterSetOperatorCharacters: filterSet = {
        itemList: [],
        container: ensuredSelector<HTMLElement>(".comparison--operator.comparison--characters"),
        controls: ensuredSelector<HTMLElement>(".comparison-header__controls.characters"),
        filter: () => filter(filterSetOperatorCharacters),
    }

    setItemList(filterSetOperatorCharacters)
    initFilterEventListeners(filterSetOperatorCharacters)
    filterSetOperatorCharacters.filter()
}

function initDetailsEventListeners() {
    initComponents()
    document.querySelectorAll<HTMLElement>(".act").forEach((e) => {
        e.querySelector(".act__details-button")!.addEventListener("click", () => {
            const data = JSON.parse(e.dataset.stages!) as infoUnlockDatas[]
            const name = e.querySelector<HTMLElement>(".act__title")!.innerText

            const modalWrapper = new ModalWrapper()
            const modal = new BaseModal(modalWrapper, new BaseModalHeader(name))
            data.forEach((stage) => {
                const wordNode = createElement("span", {
                    className: "stage-list-item__length stage-list-item__length--words",
                    attributes: { innerText: stage.count.words.toString() },
                })
                wordNode.dataset.words = stage.count.words.toString()

                const charNode = createElement("span", {
                    className: "stage-list-item__length stage-list-item__length--chars",
                    attributes: { innerText: stage.count.chars.toString() },
                })
                charNode.dataset.characters = stage.count.chars.toString()

                modal.addContent(
                    createElement("div", {
                        className: "stage-list-item",
                        children: [
                            createElement("div", {
                                className: "stage-list-item__title",
                                children: [
                                    createElement("span", { className: "stage-list-item__title-code", attributes: { innerText: `${stage.storyCode} ${stage.avgTag.replace("Operation", "")}` } }),
                                    createElement("span", { className: "stage-list-item__title-name", attributes: { innerText: stage.storyName } }),
                                ],
                            }),
                            charNode,
                            wordNode,
                        ],
                    })
                )
            })
            document.body.append(modalWrapper)
            calcWPM()
        })
    })
}

function calcWPM() {
    document.querySelectorAll<HTMLElement>("*[data-words]").forEach((e) => {
        // catch already calcuated ones
        if (e.dataset.readTime || e.dataset.words == "") {
            return
        }
        let minutes = Math.round(parseInt(e.dataset.words!) / 200)
        let hours = 0
        while (minutes > 60) {
            minutes -= 60
            hours++
        }
        e.dataset.readTime = hours == 0 ? `${minutes} minutes` : `${hours} hours ${minutes} minutes`
    })
}
