module Avatars
  def find_avatar(input)
    unknown_avatar = '/img/avatars/unknown.png'
    unknown_avatar unless input.is_a? String
    config = @context.registers[:site].config

    wanted_file = '/img/avatars/' + input.gsub("'", '').gsub(' ', '').gsub('ł', 'l') + '.png'

    File.exist?(config['source'] + wanted_file) ? wanted_file : unknown_avatar
  end
end

Liquid::Template.register_filter(Avatars)
