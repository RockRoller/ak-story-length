/**
 * story_review_table.json
 */
export type storyReviewTable = Record<string, storyReviewTableEntry>

/**
 * one entry of story_review_table
 */
export type storyReviewTableEntry = {
    /**
     * id of the story
     */
    id: string
    /**
     * name of the story
     */
    name: string
    /**
     * type of the story e.g. activity, mini_activity, mainline
     */
    entryType: string
    /**
     * list of all story stages
     */
    infoUnlockDatas: infoUnlockDatas[]
    /**
     * self-made type, any other ones are coming from AK itself
     * name of the operator
     */
    operatorName?: string
    /**
     * self-made type, any other ones are coming from AK itself
     */
    sortId?: number
}

/**
 * data of one story
 */
export type infoUnlockDatas = {
    /**
     * name of the stories stage
     */
    storyName: string
    /**
     * code of the stories stage
     */
    storyCode: string
    /**
     * text file of this story
     */
    storyTxt: string
    /**
     * story type, e.g. before, after
     */
    avgTag: string
    /**
     * self-made type, any other ones are coming from AK itself
     */
    count: count
}

/**
 * object for word + character counts
 */
export type count = {
    words: number
    chars: number
}

/**
 * handbook_info_table.json
 */
export type handbookInfoTable = {
    /**
     * operator list, lists for e.g. npc also exist but are not relevant
     */
    handbookDict: Record<string, handbookInfoTableCharacterEntry>
}

/**
 * one character entry of the handbook_info_table
 */
export type handbookInfoTableCharacterEntry = {
    /**
     * character id
     */
    charID: string
    /**
     * list of trust notes
     */
    storyTextAudio: CharacterTrustInfoEntry[]
    /**
     * list of character stories
     */
    handbookAvgList: CharacterStoryEntry[]
}

/**
 * one character stories entry
 */
export type CharacterStoryEntry = {
    /**
     * id of the character story, represent the according text file
     */
    storySetId: string
    /**
     * number of the character story, 1 for first character story of the character, 2 for second, ...
     */
    sortId: number
}

/**
 * one character trust into entry
 */
export type CharacterTrustInfoEntry = {
    /**
     * list of stories
     */
    stories: {
        /**
         * text content
         */
        storyText: string
    }[]
}

/**
 * character_table.json
 */
export type characterTable = Record<string, characterTableEntry>

/**
 * select data of one entry in the character_table.json
 */
export type characterTableEntry = {
    /**
     * name of the character
     */
    name: string
}

/**
 * remapped character stories entry to make it easier to work with in jekyll
 * represent one character grouped with all their stories
 */
export type remappedCharacterStoriesEntry = {
    /**
     * name of the character
     */
    characterName: string
    /**
     * a list of all stories this character has
     */
    stories: remappedCharacterStoryEntry[]
}

/**
 * remapped character story entry to make it easier to work with in jekyll
 * represent one character story
 */
export type remappedCharacterStoryEntry = {
    /**
     * number of the character story, 1 for first character story of the character, 2 for second, ...
     */
    id: number
    /**
     * name of the character story
     */
    name: string
    /**
     * @see count
     */
    count: count
}

/**
 * charword_table.json
 */
export type characterWordTable = {
    charWords: Record<string, characterWordTableEntry>
}

/**
 * one entry of charword_table
 */
export type characterWordTableEntry = {
    /**
     * text content
     */
    voiceText: string
    /**
     * character id
     */
    charId: string
}
